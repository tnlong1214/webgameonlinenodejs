import express, { Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import path from "path";
import expressLayouts from "express-ejs-layouts";
import methodOverrid from "method-override";
import session from "express-session";
import * as homeController from "./controllers/web/home";
import * as contactController from "./controllers/web/contact";
import * as aboutController from "./controllers/web/about";
import * as gameController from "./controllers/web/game";
import * as galleryController from "./controllers/web/gallery";
import * as detailController from "./controllers/web/detail";
import * as blogController from "./controllers/web/blog";







import logger from "morgan";
const app = express();
app.disable("view cache");


app.use(logger("dev"));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: "somesecret",
  cookie: { secure: false }
}));



app.use(expressLayouts);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverrid("_method"));
app.set("layout", "layouts/index");



app.get("/", validateStatic, homeController.index);
app.get("/contacts", validateStatic, contactController.index);
app.get("/abouts", validateStatic, aboutController.index);
app.get("/games", validateStatic, gameController.index);
app.get("/playnows", validateStatic, gameController.indexPlayNow);

app.get("/gallerys", validateStatic, galleryController.index);
app.get("/details/:id", validateStatic, detailController.index);
app.get("/blogs", validateStatic, blogController.index);
app.get("/:id/blog-details", validateStatic, blogController.blogDetails);
app.post("/pages", validateStatic, blogController.page);
app.post("/comments", validateStatic, blogController.comment);




app.get("/:name", async (req, res) => {
  try {
    app.engine("html", require("ejs").renderFile);
    app.set("views", path.join(__dirname, `../public/${req.params.name}`));
    app.set("view engine", "html");
    app.use(`/${req.params.name}`, express.static(path.join(__dirname, `../public/${req.params.name}`), { etag: false, cacheControl: false }));

    res.sendFile(path.join(__dirname, `../public/${req.params.name}/index.html`));

  }
  catch (err) {
    console.log(err);
  }

});

async function validateStatic(_req: Request, _res: Response, next: NextFunction) {
  app.use((_req: Request, _res: Response, next: NextFunction) => {

    _res.set("Cache-Control", "no-cache, no-store, must-revalidate");

    next();
  });

  app.use(express.static(path.join(__dirname, `./public`), {
    etag: false, cacheControl: false
  }));
  app.set("views", path.join(__dirname, "../views"));
  app.set("view engine", "ejs");
  next();
}


export default app;