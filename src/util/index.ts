"use strict";

export let _UTIL = {
    checkMissingKey: (obj: any, objCheck: any) => {
        console.log("checkMissingKey>>>>", obj);
        for (let i = 0; i < objCheck.length; i++) {
            if (!obj.hasOwnProperty(objCheck[i])) {
                return objCheck[i];
            }
        }

        return "okay";
    }
};