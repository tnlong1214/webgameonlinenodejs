import express, { Request, Response, NextFunction } from "express";
import compression from "compression";  // compresses requests
import bodyParser from "body-parser";
import path from "path";
import expressLayouts from "express-ejs-layouts";
import * as homeController from "./controllers/admin/home";
import * as productsController from "./controllers/admin/products";
import * as productTypesController from "./controllers/admin/product_types";
import * as postController from "./controllers/admin/posts";


import logger from "morgan";
import * as userController from "./controllers/admin/user_Controller";
import multer from "multer";
import fs from "fs";
import expressSession from "express-session";
const flash = require("connect-flash");
import cookieParser from "cookie-parser";


const storage = multer.diskStorage({
  destination: function (_req, _file, cb) {
    const dir = "game";
    const dir2 = "dist/public/images";

    if (_file.fieldname === "fileGame") {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      cb(undefined, dir);
    } else if (_file.fieldname === "image") {
      if (!fs.existsSync(dir2)) {
        fs.mkdirSync(dir2);
      }
      cb(undefined, dir2);
    }
  },
  filename: function (_req, file, cb) {

    cb(undefined, file.originalname);
  }
});

const upload = multer({
  storage: storage
});

const app2 = express();
app2.use(cookieParser());



app2.set("views", path.join(__dirname, "../views"));
app2.set("view engine", "ejs");
app2.use(logger("dev"));
app2.use(expressLayouts);
app2.use(compression());
app2.use(bodyParser.json());
app2.use(bodyParser.urlencoded({ extended: true }));
app2.use(
  express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);
app2.use(expressSession({
  secret: "mySecretKey",
  resave: true,
  saveUninitialized: true
}));
app2.use(flash());
const sessionFlash = function (req: Request, res: Response, next: NextFunction) {
  res.locals.currentUser = req.user;
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  next();

};
app2.use(sessionFlash);

app2.set("layout", "layouts/admin");
app2.get("/login", userController.getLogin);
app2.post("/login", userController.postLogin);
app2.get("/signup", userController.getSignup);
app2.post("/signup", userController.postSignup);
app2.get("/logout", userController.logout);
app2.get("/change-passwords", userController.tokenGuard(), userController.reset);
app2.post("/change-passwords", userController.tokenGuard(), userController.savePassword);


app2.get("/", userController.tokenGuard(), homeController.index);
app2.get("/news/add", userController.tokenGuard(), postController.addNew);
app2.get("/news", userController.tokenGuard(), postController.index);


app2.post("/news", userController.tokenGuard(), upload.fields([{
  name: "fileGame",
  maxCount: 1
},
{
  name: "image", maxCount: 1
}
]), postController.create);


app2.get("/products-types", userController.tokenGuard(), productTypesController.index);
app2.post("/products-types", userController.tokenGuard(), productTypesController.addNew);
app2.get("/products-types/:id/delete", userController.tokenGuard(), productTypesController.deletePrt);
app2.get("/product", userController.tokenGuard(), productsController.index);
app2.get("/product/add", userController.tokenGuard(), productsController.add);
app2.get("/product/:id/delete", userController.tokenGuard(), productsController.deletePro);
app2.get("/product/:id/edit", userController.tokenGuard(), productsController.editPro);
app2.post("/product/:id/edit", userController.tokenGuard(), productsController.saveUpdate);
app2.post("/product", userController.tokenGuard(), upload.fields([{
  name: "fileGame",
  maxCount: 1
},
{
  name: "image", maxCount: 1
}
]), productsController.addNew);




export default app2;