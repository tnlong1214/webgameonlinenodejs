
import {

  Model,
  DataTypes,

} from "sequelize";
import { sequelize } from "../config/database";


interface PostInstance extends Model {
  id: number;
  title: string;
  content: Text;
  img: string;
}



export const Post = sequelize.define<PostInstance>("post", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  title: {
    type: DataTypes.STRING

  },
  content: {
    type: DataTypes.TEXT
  },
  img: {
    type: DataTypes.STRING
  }


});