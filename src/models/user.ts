
import {

  Model,
  DataTypes,

} from "sequelize";
import { sequelize } from "../config/database";


interface UserInstance extends Model {
  [x: string]: any;
  id: number;
  username: string;
  email: string;
  password: string;
  roles: number;
}

export interface UserAddModel {
  email: string;
  password: string;
}

export const User = sequelize.define<UserInstance>("user", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  username: {
    type: DataTypes.STRING,
    defaultValue: "moderator"

  },
  email: {
    type: DataTypes.STRING
  },
  password: {
    type: DataTypes.STRING
  },
  roles: {
    type: DataTypes.JSON,
    defaultValue: 0
  },


});