
import {

  Model,
  DataTypes,

} from "sequelize";
import { sequelize } from "../config/database";


interface ProductInstance extends Model {
  product_id: number;
  name: string;
  price: number;
  description: string;
  image: string;
  url: string;
  category: number;
}

export const Product = sequelize.define<ProductInstance>("product", {
  product_id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING

  },
  price: {
    type: DataTypes.INTEGER
  },
  description: {
    type: DataTypes.STRING
  },
  image: {
    type: DataTypes.STRING
  },
  url: {
    type: DataTypes.STRING
  },
  category: {
    type: DataTypes.INTEGER
  },


});