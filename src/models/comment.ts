
import {

  Model,
  DataTypes,

} from "sequelize";
import { sequelize } from "../config/database";


interface CommentInstance extends Model {
  id: number;
  id_post: string;
  content: Text;
  email: string;
  name: string;

}



export const Comment = sequelize.define<CommentInstance>("comment", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  id_post: {
    type: DataTypes.INTEGER

  },
  content: {
    type: DataTypes.TEXT
  },
  email: {
    type: DataTypes.STRING
  },
   name: {
    type: DataTypes.STRING
  }


});