
import {

  Model,
  DataTypes,

} from "sequelize";
import { sequelize } from "../config/database";


interface ProductTypesInstance extends Model {
  product_id: number;
  name: string;

}

export const ProductType = sequelize.define<ProductTypesInstance>("product_type", {
  id: {
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING

  }


});