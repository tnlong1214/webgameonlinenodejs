import { Request, Response } from "express";
import { Product } from "../../models/product";

export let index = async (_req: Request, _res: Response) => {
  const results = await select();
  _res.render("web/router/game", {
    title: "Game",
    results,
  });
};

export let indexPlayNow = async (_req: Request, _res: Response) => {
  const results = await select();
  _res.render("web/router/playnow", {
    title: "Play Now",
    results,
  });
};



const select = async () => {
  return new Promise((resolve, reject) => {
    Product.findAll().then(data => {
      resolve(data);
    })
      .catch(err => {
        console.log(err);
        reject(undefined);

      });

  });
};
