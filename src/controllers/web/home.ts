import { Request, Response } from "express";
import { Product } from "../../models/product";
import { ProductType } from "../../models/product_types";



export let index = async (_req: Request, _res: Response) => {
  const results = await select();
  const types = await selectTypes();
  _res.render("web/router/home", {
    title: "Product",
    results,
    types
  });
};

const selectTypes = async () => {
  return new Promise((resolve, reject) => {
    ProductType.findAll().then(data => {
      return resolve(data);
    })

      .catch(err => {
        if (err) return reject(undefined);
      });

  });
};


const select = async () => {
  return new Promise((resolve, reject) => {
    Product.findAll().then(data => {
      resolve(data);
    })
      .catch(err => {
        console.log(err);
        reject(undefined);

      });

  });
};