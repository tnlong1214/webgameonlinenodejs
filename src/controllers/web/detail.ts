import { Request, Response } from "express";
import { Identifier } from "sequelize/types";
import { Product } from "../../models/product";

export let index = async (_req: Request, _res: Response) => {
  const id = _req.params.id;
  const data = await select(id);
  _res.render("web/router/details", {
    title: "Detail",
    data,
  });
};


const select = async (id: Identifier) => {
  return new Promise((resolve, reject) => {
    Product.findByPk(id).then(data => {
      resolve(data);
    })
      .catch(err => {
        console.log(err);
        reject(undefined);

      });

  });
};
