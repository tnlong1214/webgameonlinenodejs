import { Request, Response } from "express";
import { Product } from "../../models/product";

export let index = async (_req: Request, _res: Response) => {
  const data = await select();

  _res.render("web/router/gallery", {
    title: "Gallery",
    data,
  });
};


const select = async () => {
  return new Promise((resolve, reject) => {
    Product.findAll().then(data => {
      resolve(data);
    })
      .catch(err => {
        console.log(err);
        reject(undefined);

      });

  });
};
