import { Request, Response } from "express";
import { Post } from "../../models/post";
import { Comment } from "../../models/comment";


export let index = async (_req: Request, _res: Response) => {


  _res.render("web/router/blog", {
    title: "Blog",
    page: 0
  });
};


export let blogDetails = async (_req: Request, _res: Response) => {
  const id = _req.params.id;
  const comment = await Comment.findAll({
  where: {
    id_post: id
  }
});
  const data = await Post.findByPk(id);
  _res.render("web/router/blog_details", {
    title: "Blog",
    data,
    comment
  });
};


export let page = async (_req: Request, _res: Response) => {
  const page = parseInt(_req.body.page) || 0;
  const limit = 4;
  const offset = page ? page * limit : 0;
  const data = await Post.findAll({
    offset: offset,
    limit: limit,
    order: [
      ["id", "ASC"]
    ]
  });
  _res.json(data);

};



export let comment = async (_req: Request, _res: Response) => {
  const comment = {
    id_post: _req.body.id_post,
    email: _req.body.email,
    name: _req.body.name,
    content: _req.body.content
  };
  console.log(comment);
    const save = await Comment.create(comment);

    if (save) {
  return  _res.redirect(`/${_req.body.id_post}/blog-details`);
}
};