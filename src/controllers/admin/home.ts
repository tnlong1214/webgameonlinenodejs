import { Request, Response } from "express";
import { Post } from "../../models/post";
import { Product } from "../../models/product";
import { Comment } from "../../models/comment";


export let index = async (_req: Request, _res: Response) => {
  const products = await Product.findAll();
  const posts = await Post.findAll();
  const comment = await Comment.findAll();
  console.log(comment);
  _res.render("admin/router/home", {
    user: _res.locals.currentUser,
    title: "Home",
    page_name: "home"
    , products,
    posts,
    comment
  });
};

