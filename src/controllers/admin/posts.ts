import { Request, Response } from "express";
import { Post } from "../../models/post";

interface MulterRequest extends Request {
  files: any;
}

export let addNew = (_req: Request, _res: Response) => {
  _res.render("admin/router/add_new", {
    user: _res.locals.currentUser,
    title: "Post",
    page_name: "page"
  });
};

export let index = async (_req: Request, _res: Response) => {
  const data = await Post.findAll();
  _res.render("admin/router/new", {
    user: _res.locals.currentUser,
    title: "Post",
    data,
    message: _res.locals.success_msg,
    page_name: "page"
  });
};



export let create = async (_req: MulterRequest, _res: Response) => {
  const fileImage = _req.files.image[0];

  if (!fileImage) {
    return _res.send({
      status: 201,
      url: ("Please upload a file")
    });
  } else {
    const data = {
      title: _req.body.title,
      content: _req.body.editor1,
      img: "images/" + fileImage.originalname,
    };
    const save = await Post.create(data);
    if (save) {
      return _res.redirect("/");
    }
  }

};

