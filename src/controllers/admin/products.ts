import { Request, Response } from "express";
import fs from "fs";
import unzipper from "unzipper";

import { Product } from "../../models/product";
import { ProductType } from "../../models/product_types";

import { body, validationResult } from "express-validator";

import path from "path";

interface MulterRequest extends Request {
  files: any;
}




export let index = async (_req: Request, _res: Response) => {
  const data = await select();


  _res.render("admin/router/product", {
    user: _res.locals.currentUser,

    title: "Product",
    data,
    message: _res.locals.success_msg,
    page_name: "product"
  });
};



export let add = async (_req: Request, _res: Response) => {
  const data = await selectTypes();
  _res.render("admin/router/add", {
    user: _res.locals.currentUser,

    title: "Product",
    message: _res.locals.error,
    data,
    page_name: "product"
  });
};

export let deletePro = async (_req: Request, _res: Response) => {
  const product_id = _req.params.id;
  try {
    const product = await Product.findOne({ where: { product_id } });
    const nameGame = await (<string>product.url).split(/[\/]/)[1];
    await Product.destroy({ where: { product_id } })
      .then(async function (rowDeleted) {
        if (rowDeleted === 1) {
          const dir = __dirname + `/../../../public/${nameGame}/`;
          await fs.rmdir(dir,  (error) => {
            if (error) {
              console.log(error);
            }
            else {
              console.log("Recursive: Directories Deleted!");
              const dirImage = __dirname + `/../../public/${product.image}`;
              fs.rmdir(dirImage, (error) => {
                if (error) {
                  console.log(error);
                }
                else {
                  console.log("Recursive: Directories image!");
                  const dirZip = __dirname + `/../../../game/${nameGame}.zip`;
                  fs.rmdir(dirZip, (error) => {
                    if (error) {
                      console.log(error);
                    }
                    else {
                      console.log("Recursive: Directories zip!");
                    }
                  });
                }
              });
            }
          });
          const success = "Deleted successfully";
          _res.locals.success_msg = _req.flash("success_msg", success);
          return _res.redirect("/product");
        }
      }, function (err) {
        console.log(err);
      });
  } catch (err) {
    console.error(`Error while deleting .`);
  }


};

export let editPro = async (_req: Request, _res: Response) => {
  const data = await selectTypes();
  _res.render("admin/router/edit", {
    user: _res.locals.currentUser,

    title: "Product Edit ",
    data,
    id: _req.params.id,
    error: _res.locals.error,
    page_name: "product"
  });
};


export let saveUpdate = async (_req: Request, _res: Response) => {
  try {

    const product_id = _req.params.id;
    Product.update({ name: _req.body.exampleGameName, price: _req.body.price, description: _req.body.exampleDescription, category: _req.body.select }, { where: { product_id } }).then(rowUpdate => {
      if (rowUpdate[0] === 1) {
        const success = "Edit successfully";
        _res.locals.success_msg = _req.flash("success_msg", success);
        return _res.redirect("/product");
      }
    });
  } catch (error) {
    console.log(error);
  }

};



export const selectTypes = async () => {
  return new Promise((resolve, reject) => {
    ProductType.findAll().then(data => {
      return resolve(data);
    })

      .catch(err => {
        if (err) return reject(undefined);
      });

  });
};


export let addNew = async (_req: MulterRequest, _res: Response) => {
  try {
    await body("exampleGameName").custom(name => {
      return Product.findOne({ where: { name } }).then(product => {
        if (product) {
          return Promise.reject("Name is registered");
        }
      });
    }).run(_req);

    const errors: any = validationResult(_req);
    if (!errors.isEmpty()) {
      console.log(errors);
      const errMsgs = errors.array().map((err: { msg: any; }) => err.msg);
      _res.locals.error = _req.flash("error", errMsgs);
      return _res.redirect(`/product/add`);

    }


    const file = _req.files.fileGame[0];
    const fileImage = _req.files.image[0];

    if (!file || !fileImage) {
      return _res.send({
        status: 201,
        url: ("Please upload a file")
      });
    } else {
      const string = await file.originalname;
      const nameGame = await (<string>string).split(/[,.\/]/)[0];
      const pathHtml: String = path.join(__dirname, `../../../public/${nameGame}/index.html`);
      const pathNew: String = `<base href="/${nameGame}/"> `;
      const pathTxt: String = path.join(__dirname, `../../../public/index.html`);
      return unzip(file.path, nameGame).then(async function (res: number) {
        if (res == 200) {
          const copy = await createFile(pathHtml, pathNew, pathTxt);
          if (copy) {
            return copy;
          }
        } else {
          const errMsgs = "Error Unzip";
          _res.locals.error = _req.flash("error", errMsgs);
          return _res.redirect(`/product/add`);
        }
      })
        .then(async function (res: number) {
          const product = {
            name: _req.body.exampleGameName,
            price: _req.body.price,
            description: _req.body.exampleDescription,
            category: _req.body.select,
            image: "images/" + fileImage.originalname,
            url: `/${nameGame}`
          };
          console.log(res);
          console.log(product);
          if (res == 200) {
            const save = await Product.create(product);
            if (save) {
              return _res.redirect("/product");
            }
          } else {
            const errMsgs = "Error createFile";
            _res.locals.error = _req.flash("error", errMsgs);
            return _res.redirect(`/product/add`);
          }
        })
        .catch(async (err) => {
          const errMsgs = "Error Unzip  " + err;
          _res.locals.error = _req.flash("error", errMsgs);
          return _res.redirect(`/product/add`);
        });
    }
  } catch (err) {

    console.log(err);
  }


};


function unzip(fileContents: string, name: string) {
  console.log("unzip");

  return new Promise((resolve, reject) => {
    try {
      const dir = `/public/${name}`;
      if (!fs.existsSync(dir)) {
        fs.mkdir(dir, _err => { });
      }
      fs.createReadStream(fileContents).pipe(unzipper.Extract({ path: `public/${name}` })).on("close", () => {
        return resolve(200);
      });
    } catch (error) {
      reject(error);
    }

  });
}




const createFile = async (pathHtml: String, pathNew: String, pathTxt: String) => {



  const p1 = await new Promise((resolve, reject) => {
    fs.copyFile(`${pathHtml}`, `${pathTxt}`, (err) => {
      console.log("copyFile");
      if (err) return reject(undefined);
      resolve("1");
    });
  });

  const p2 = await new Promise((resolve, reject) => {
    fs.writeFile(`${pathHtml}`, pathNew, function (err) {
      console.log("writeFile");

      if (err) return reject(undefined);
      resolve("2");
    });
  });

  const p3 = await new Promise((resolve, reject) => {
    fs.readFile(`${pathTxt}`, function (err, data) {
      if (err) return reject(undefined);
      console.log("File was read");
      fs.appendFile(`${pathHtml}`, data, function (err) {
        if (err) return reject(undefined);
        console.log('The "data to append" was appended to file!');
        return resolve(200);
      });
    });
  }
  );
  return Promise.all([p1, p2, p3]).then(values => {
    console.log("values" + values);
    return 200;
  }).catch(reason => {
    console.log("reason" + reason);
    return undefined;
  });
};



const select = async () => {
  return new Promise((resolve, reject) => {

    Product.findAll().then(data => {
      resolve(data);
    })
      .catch(err => {
        console.log(err);
        reject(undefined);

      });


  });
};

